require_relative 'game'

class HumanPlayer
  attr_reader :name
  attr_accessor :mark

  def initialize(name)
    @name = name
    @mark = :X
  end

  def get_move
    puts 'Where would you like to play? (1-9)'
    num_to_move(gets.chomp.to_i)
  end

  def display(board = Game.board)
    board.graphic
  end

  private
  def num_to_move(num)
    moves = {
      1 => [0, 0],
      2 => [0, 1],
      3 => [0, 2],
      4 => [1, 0],
      5 => [1, 1],
      6 => [1, 2],
      7 => [2, 0],
      8 => [2, 1],
      9 => [2, 2]
    }
    moves[num]
  end
end
