require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_reader :board, :current_player

  def initialize(p_one, p_two)
    @player_one = p_one
    @player_two = p_two
    @board = Board.new
    @current_player = @player_one
  end

  def switch_players!
    @current_player = (@current_player == @player_one ? @player_two : @player_one)
  end

  def play_turn
    @current_player.display(@board)
    @board.place_mark(@current_player.get_move, current_player.mark)
    switch_players!
  end

end

def player_maker
  name = gets.chomp
  puts "Is this player human? Y/N"
  human = (gets.chomp.downcase == 'y' ? true : false)
  (human ? HumanPlayer.new(name) : ComputerPlayer.new(name))
end

def start_game
  puts "What is player one's name?"
  p_one = player_maker
  puts "Would player one like to be X's or O's?"
  mark = gets.chomp
  puts "What is player two's name?"
  p_two = player_maker
  p_one.mark = (mark.downcase == 'x' ? :X : :O)
  p_two.mark = (mark.downcase == 'x' ? :O : :X)
  Game.new(p_one, p_two)
end

if __FILE__ == $PROGRAM_NAME
  game = start_game
  until game.board.over?
    game.play_turn
  end
  game.board.graphic
  puts "Congratulations! #{game.board.winner} wins!"
end
