require_relative 'board'
require_relative 'game'

class ComputerPlayer
  attr_reader :board
  attr_accessor :mark

  def initialize(name)
    @name = name
    @mark = :O
  end

  def display(board)
    @board = board
  end

  def get_move
    grid = @board.grid
    grid.each_index do |i|
      grid[i].each_index do |j|
        grid[i][j] == nil ? grid[i][j] = @mark : next
        @board.winner(grid) ? (return [i, j]) : grid[i][j] = nil
      end
    end
    x = rand(3)
    y = rand(3)
    until @board.empty?([x, y])
      x = rand(3)
      y = rand(3)
    end
    [x, y]
  end

end
