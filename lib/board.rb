class Board
  attr_reader :grid

  def initialize(grid = [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]])
    @grid = grid
  end

  def place_mark(pos, mark)
    @grid[pos[0]][pos[1]] = mark
  end

  def winner(grid=@grid)
    %i[X O].each do |n|
      grid.each_index { |i| return n if grid[i].all? { |x| x == n } }
      grid.each_index do |i|
        return n if (grid[0][i] == n) && (grid[1][i] == n) && (grid[2][i] == n)
      end
      return n if (grid[0][0] == n) && (grid[1][1] == n) && (grid[2][2] == n)
      return n if (grid[0][2] == n) && (grid[1][1] == n) && (grid[2][0] == n)
    end
    nil
  end

  def graphic
    v_grid = @grid
    v_string = "\n
    1 | 2 | 3\n
    4 ] 5 [ 6\n
    7 | 8 | 9\n"
    (1..9).each do |n|
      pos = num_to_move(n)
      next unless v_grid[pos[0]][pos[1]]
      v_string.sub!("#{n}", v_grid[pos[0]][pos[1]].to_s)
    end
    puts v_string
    v_string
  end

  def num_to_move(num)
    moves = {
      1 => [0, 0],
      2 => [0, 1],
      3 => [0, 2],
      4 => [1, 0],
      5 => [1, 1],
      6 => [1, 2],
      7 => [2, 0],
      8 => [2, 1],
      9 => [2, 2]
    }
    moves[num]
  end

  def over?
    return true if !winner.nil?
    @grid.each_index { |i| return false if @grid[i].include? nil }
    true
  end

  def empty?(pos)
    @grid[pos[0]][pos[1]].nil?
  end

end
